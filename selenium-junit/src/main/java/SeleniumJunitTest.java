import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.*;
import org.testcontainers.containers.BrowserWebDriverContainer;

import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;


@Testcontainers
public class SeleniumJunitTest {

    private final String EMAIL = "andrey.kravchenko@objectstyle.com";
    private final String PASSWORD = "";

    @Rule
    public BrowserWebDriverContainer<?> chrome = new BrowserWebDriverContainer<>(DockerImageName.parse("selenium/standalone-chrome:latest"))
            .withCapabilities(new ChromeOptions());

    @Test
    public void test() {
        RemoteWebDriver driver = chrome.getWebDriver();

        driver.get("https://editor.staging.objectstyle.com/articles");

        WebElement webElement = driver.findElement(By.xpath("//a[@class='button is-primary']"));
        webElement.click();

        Wait<RemoteWebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);

        fluentWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='identifierId']"))).sendKeys(EMAIL);
        driver.findElement(By.id("identifierNext")).click();
        fluentWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='password']"))).sendKeys(PASSWORD);
        driver.findElement(By.id("passwordNext")).click();

        try {
            Thread.sleep(900);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (!driver.getCurrentUrl().endsWith("articles")) {
            driver.get("https://editor.staging.objectstyle.com/articles");
        }

        List<WebElement> webElements = fluentWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@id=\"root\"]/div/div[2]/table/tbody/tr")));
        if (webElements.size() > 0) {
            String title1 = fluentWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[2]/table/tbody/tr[1]/th[3]"))).getText();
            webElements.get(0).click();

            String title2 = fluentWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[2]/form/div[2]/div/input"))).getAttribute("value");
            assertEquals(title1, title2);
        } else {
            System.out.println("Table is empty!");
        }

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/nav/div/div[2]/a[10]")));
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();" , element);

        assertTrue(driver.getCurrentUrl().endsWith("login"));
    }

    @After
    public void printExitTime() {
        chrome.stop();
    }

}
